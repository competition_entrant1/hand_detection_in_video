import glob
import os
import aps
from aps import GetBoxes
import json
import math

from pycocotools.coco import COCO
import numpy as np
import cv2

tr_path = "/home/[REDACTED]/coco/annotations/person_keypoints_train2014.json"
val_path = "/home/[REDACTED]/coco/annotations/person_keypoints_val2014.json"
coco = None
catIds = None

IMG_DIR = "/home/[REDACTED]/coco/train2014"
RESULTS_DIR = "/home/[REDACTED]/project/cmu_results_coco"

def imgtoresult(path):
    return os.path.join(RESULTS_DIR, os.path.basename(path)[:-4]+"_keypoints.json")
def pathtoid(path):
    if "train2014" in path:
        return int(os.path.basename(path)[15:15+12])
    else: # "val2014" in path
        return int(os.path.basename(path)[13:13+12])
def idtopath(pid):
    p = os.path.join(IMG_DIR, "COCO_train2014_{}.jpg".format(pid))
    if not os.path.exists(p):
        p = os.path.join(IMG_DIR, "../val2014", "COCO_val2014_{}.jpg".format(pid))
    return p
def dist(x, y):
    return ((x[0]-y[0])**2 + (x[1]-y[1])**2)**.5


def get_gt(pid):
    global coco, catIds, coco_tr, coco_val, coco_tr_ids, coco_val_ids
    if coco is None or catIds is None:
        coco = COCO("/home/[REDACTED]/coco/annotations/instances_train2014.json")
        catIds = coco.getCatIds(catNms=['person']);
        coco_tr = COCO(tr_path)
        coco_val = COCO(val_path)
        coco_tr_ids = coco_tr.getImgIds(catIds=catIds)
        coco_val_ids = coco_val.getImgIds(catIds=catIds)

    pid = int(pid)
    if pid in coco_tr_ids:
        annIds = coco_tr.getAnnIds(imgIds=pid, catIds=catIds, iscrowd=None)
        anns = coco_tr.loadAnns(annIds)
    elif pid in coco_val_ids:
        annIds = coco_val.getAnnIds(imgIds=pid, catIds=catIds, iscrowd=None)
        anns = coco_val.loadAnns(annIds)
    else:
        #raise Exception("PID not found")
        return []
    g = {}
    for p in anns:
        if p["category_id"] != 1: continue
        for x in range(15):
            if x not in g: g[x] = []
            if p["keypoints"][3*x+2] == 2:
                g[x].append((p["keypoints"][3*x], p["keypoints"][3*x+1]))
            if p["keypoints"][3*x+2] == 2:
                g[x].append((p["keypoints"][3*x], p["keypoints"][3*x+1]))
    return g
def get_gt_wrists(pid):
    x = get_gt(pid)
    if len(x) <= 10:
        return []
    return x[9] + x[10]
def draw(img, pts):
    for k in pts:
        cv2.circle(img, k, 4, (255, 0, 255), -1)
    return img


def get_pred_wrists(pid):
    pid = int(pid)
    p = os.path.join(RESULTS_DIR, "COCO_train2014_{:012}_keypoints.json".format(pid))
    if not os.path.isfile(p):
        p = os.path.join(RESULTS_DIR, "COCO_val2014_{:012}_keypoints.json".format(pid))
    try:
        a = json.loads(open(p,"r").read())
    except:
        print(p)
        print(open(p,"r").read())
        a = json.loads(open(p,"r").read())
        print(a)
    wrists = []
    for p in a["people"]:
        x, y = (p["pose_keypoints"][3*4], p["pose_keypoints"][3*4+1])
        x2, y2 = (p["pose_keypoints"][3*7], p["pose_keypoints"][3*7+1])
        wrists.append((x,y))
        wrists.append((x2,y2))
    return wrists

def rot(pt, angle):
    return (math.cos(angle)*pt[0]-math.sin(angle)*pt[1],
            math.sin(angle)*pt[0]+math.cos(angle)*pt[1])

def get_pred_hand_box(wrist, hand_pts):
    xs = []
    ys = []
    for i, p in enumerate(hand_pts):
        if i % 3 == 0: xs.append(p)
        elif i % 3 == 1: ys.append(p)
    x_avg = sum(xs)/len(xs)
    y_avg = sum(ys)/len(ys)
    wh = (x_avg-wrist[0], y_avg-wrist[1])
    ang = math.atan2(wh[1], wh[0]) - 3.1415926536/2
    mag = math.sqrt(wh[0]**2 + wh[1]**2)
    rot_w = rot(wh, -ang)
    assert abs(rot_w[0]) < 1e-6
    rot_pts = []
    xmin = 1e9
    xmax = ymax = -1e9
    for x, y in zip(xs, ys):
        rot_pts.append(rot((x-wrist[0], y-wrist[1]), -ang))
    for x, y in rot_pts:
        xmin = min(x, xmin)
        xmax = max(x, xmax)
        ymax = max(y, ymax)
    A = rot((xmin, 0), ang)
    B = rot((xmin, ymax), ang)
    C = rot((xmax, ymax), ang)
    D = rot((xmax, 0), ang)
    A = (A[0]+wrist[0], A[1]+wrist[1])
    B = (B[0]+wrist[0], B[1]+wrist[1])
    C = (C[0]+wrist[0], C[1]+wrist[1])
    D = (D[0]+wrist[0], D[1]+wrist[1])
    return [A, B, C, D]
 
def get_pred_boxes(path):
    try:
        a = json.loads(open(path, "r").read())
    except:
        print(path)
        print(open(path, "r").read())
        a = json.loads(open(path, "r").read())
    b = []
    for p in a["people"]:
        x = p["hand_right_keypoints"]        
        y = p["hand_left_keypoints"]
        wr = (p["pose_keypoints"][4*3], p["pose_keypoints"][4*3+1])
        wr2 = (p["pose_keypoints"][7*3], p["pose_keypoints"][7*3+1])
        if sum(x) != 0:
            w = get_pred_hand_box(wr, x)
            b.append((w, wr2))
        if sum(y) != 0:
            w = get_pred_hand_box(wr2, y)
            b.append((w, wr2))
    return b
            
def get_boxes_within(path, ratio=0.2):
    pred = get_pred_boxes(path)
    pid = pathtoid(path)
    tru = get_gt_wrists(pid)
    boxes = []
    for bx, wr in pred:
        d = dist(bx[0], bx[1])
        m = 2e15
        for twr in tru:
            m = min(m, dist(twr, wr))
        if m/d <= ratio:
            boxes.append(bx)
    return boxes

if __name__ == "__main__":
    v = 0
    for i in glob.glob(os.path.join(RESULTS_DIR, "*")):
        if os.path.exists(os.path.join("/home/[REDACTED]/project/cmu_results_filtered", os.path.basename(i)[15:15+12])): continue
        '''p = get_pred_boxes(i)
        s = ""
        for pr in p:
            for pt in pr[0]:
                s += "{} {} ".format(*pt)
            s += "{} {}\n".format(*pr[1])
        with open(os.path.join("/home/[REDACTED]/project/cmu_results_rot", os.path.basename(i)[:-4]+"txt"), "w") as f:
            f.write(s)'''
        b = get_boxes_within(i)
        if len(b) == 0: continue
        pid = os.path.basename(i)[15:15+12]
        with open(os.path.join("/home/[REDACTED]/project/cmu_results_filtered", pid),"w") as f:
            for bx in b:
                l = []
                for pt in bx:
                    l.append(str(pt[0]))
                    l.append(str(pt[1]))
                f.write(" ".join(l))
                f.write("\n")

        v += 1
        if v % 5000 == 0: print(v)


