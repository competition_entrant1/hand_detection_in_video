import ast, glob, os, sys

import numpy as np
import scipy.io as sio
from sklearn.metrics import average_precision_score, precision_recall_curve
import matplotlib.pyplot as plt

def get_trues(video_id):
	IMG_DIR = "/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/clips"
	ANNOTATION_DIR = "/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/annos"
	frame_num = sorted(os.listdir(os.path.join(IMG_DIR, video_id)))[0]
	annotation = sio.loadmat(os.path.join(ANNOTATION_DIR, video_id, frame_num, "frm_008_hand.mat"))

	res = []

	for x in annotation['boxes']:
		l = [(int(x[0][0][0][v][0][0]), int(x[0][0][0][v][0][1])) for v in range(4)]
		#### REPLACE HERE AS NECESSARY ###
		res.append(((l[0][0]+l[1][0])/2, (l[0][1]+l[1][1])/2))

		#########
	return res

def calc_hits(trues, preds, tr=10):
	taken = {}
	y_true = []
	y_scores = []
	for pred in preds:
		m_dist = 2e15
		pr = None
		for true in trues:
			if true in taken: continue
			d = (true[1]-pred[1])**2 + (true[0]-pred[0])**2
			if d < min(tr*tr, m_dist):
				m_dist = d
				pr = true
		if pr is None:
			y_true.append(0)
		else:
			y_true.append(1)
			taken[pr] = 1
		y_scores.append(pred[2])
	for true in trues:
		if true not in taken:
			y_true.append(1)
			y_scores.append(0)
	return (y_true, y_scores)
	
if __name__ == "__main__":
	if len(sys.argv) > 1:
		x = sys.argv[1]
	else:
		x = raw_input("Video id: ")
	y = ast.literal_eval(open(os.path.join("results", x), "r").read())

	preds = sorted(y[4]+y[7], key=lambda x: 1-x[2])
	trues = get_trues(x)
	print("trues --------------------")
	print(trues)

	(y_true, y_scores) = calc_hits(trues, preds)

	print("default threshold y_true: "+str(y_true))	

	print("y_scores -----------------")
	print(y_scores)

	if len(y_scores) == 0:
		print("y_scores is empty")
		exit(0)
	if len(trues) == 0:
		print("trues is empty")
		exit(0)

	ap = average_precision_score(y_true, y_scores)
	precision, recall, _ = precision_recall_curve(y_true, y_scores)
	
	plt.plot(recall, precision, label="Precision/Recall, AP={}".format(ap))
	plt.legend(loc="lower left")
	plt.xlim([0, 1.05])
	plt.ylim([0, 1.05])
	plt.xlabel("Recall")
	plt.ylabel("Precision")
	plt.title("Precision vs. Recall, default threshold")
	plt.show()

	aps = []
	thresholds = list(range(1, 101))
	for i in thresholds:
		ap = average_precision_score(*calc_hits(trues, preds, i))
		aps.append(ap)
	plt.plot(thresholds, aps, label="Threshold/AP")
	plt.legend(loc="lower left")
	plt.ylim([0, 1.05])
	plt.xlabel("Threshold")
	plt.ylabel("AP")
	plt.show()	
