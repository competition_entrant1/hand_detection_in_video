scale_flow = 1 # 5
percent_training = 1 #.7
augment = 8 # 1-15, <=8 => no augment
channels = [6, 'M', 7, 'M', 8, 'M', 9, 'M', 10, 'M']
USE_LF = True
args.flow_weights = "weights/N6M7M8M9M10M/ssd300_0712_iter_100000.pth"
args.rgb_weights = "weights/ssd300_0712_iter_100000.pth"
