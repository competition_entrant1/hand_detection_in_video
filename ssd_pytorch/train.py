from __future__ import print_function
import os
import torch
torch.backends.cudnn.enabled = False # temporary fix for memory issue
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn
import torch.nn.init as init
import argparse
from torch.autograd import Variable
import torch.utils.data as data
from data import v2, v1, AnnotationTransform, VOCDetection, detection_collate, VOCroot, TVHands, BaseTransform
from utils.augmentations import SSDAugmentation
from my_utils import get_save_dir
from layers.modules import MultiBoxLoss
import numpy as np
import time

def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

parser = argparse.ArgumentParser(description='Single Shot MultiBox Detector Training')
parser.add_argument('--version', default='v2', help='conv11_2(v2) or pool6(v1) as last layer')
parser.add_argument('--basenet', default='vgg16_reducedfc.pth', help='pretrained base model')
parser.add_argument('--jaccard_threshold', default=0.5, type=float, help='Min Jaccard index for matching')
parser.add_argument('--batch_size', default=16, type=int, help='Batch size for training')
parser.add_argument('--resume', default=None, type=str, help='Resume from checkpoint')
parser.add_argument('--num_workers', default=4, type=int, help='Number of workers used in dataloading')
parser.add_argument('--iterations', default=120000, type=int, help='Number of training iterations')
parser.add_argument('--start_iter', default=0, type=int, help='Begin counting iterations starting from this value (should be used with resume)')
parser.add_argument('--cuda', default=True, type=str2bool, help='Use cuda to train model')
parser.add_argument('--lr', '--learning-rate', default=0.00005, type=float, help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, help='momentum')
parser.add_argument('--weight_decay', default=5e-4, type=float, help='Weight decay for SGD')
parser.add_argument('--gamma', default=0.1, type=float, help='Gamma update for SGD')
parser.add_argument('--log_iters', default=True, type=bool, help='Print the loss at each iteration')
parser.add_argument('--visdom', default=False, type=str2bool, help='Use visdom to for loss visualization')
parser.add_argument('--send_images_to_visdom', type=str2bool, default=False, help='Sample a random image from each 10th batch, send it to visdom after augmentations step')
parser.add_argument('--save_folder', default='weights/', help='Location to save checkpoint models')
parser.add_argument('--voc_root', default=VOCroot, help='Location of VOC root directory')
parser.add_argument('--save_val_every', default=2000, type=int, help='Save and validate every X iterations')
parser.add_argument('--seed', default=123, type=int, help='RNG seed')
parser.add_argument('--flow_weights', default=None, type=str, help='For LF only')
parser.add_argument('--rgb_weights', default=None, type=str, help='For LF only')
parser.add_argument('-of', '--options_file', type=str, help='Supplies options such as channels')
args = parser.parse_args()


torch.manual_seed(args.seed)
if args.cuda and torch.cuda.is_available():
    torch.cuda.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')

num_classes = 2
batch_size = args.batch_size
max_iter = 500000
weight_decay = 0.0005
stepvalues = (40000, 80000, 100000, 120000)
gamma = 0.1
momentum = 0.9
USE_V3 = False
USE_DF = False
GT_SCALE = 1
kernel_sz = 3
exec(open(args.options_file, "r").read())

if args.resume is not None:
    if args.flow_weights is not None:
        print("WARNING: SETTING FLOW_WEIGHTS TO NONE SINCE RESUME IS SPECIFIED")
    if args.rgb_weights is not None:
        print("WARNING: SETTING RGB_WEIGHTS TO NONE SINCE RESUME IS SPECIFIED")
    args.flow_weights = None
    args.rgb_weights = None

if USE_LF:
    from ssd_LF import build_ssd
    print("Using LF")
elif USE_V3:
    from ssd_v3 import build_ssd
    print("Using v3")
elif USE_DF:
    from ssd_DF import build_ssd
    print("Using DF")
else:
    from ssd import build_ssd


channels_sh, num_channels, no_rgb = get_save_dir(channels, scale_flow, percent_training, late_fusion=USE_LF,
    augment=augment, use_v3=USE_V3, direct_fusion=USE_DF, gt_scale=GT_SCALE, v3_kernel_sz=kernel_sz)
args.save_folder = os.path.join(args.save_folder, channels_sh)

if not os.path.exists(args.save_folder):
    os.makedirs(args.save_folder)

print(args.save_folder)

if args.visdom:
    import visdom
    viz = visdom.Visdom()

if no_rgb:
    means = (0,)*(num_channels+3)
else:
    means = (44.31959941978824, 47.74161072727923, 58.569580093625135) + (0,)*(num_channels-3)
size = 300

if USE_LF and (args.rgb_weights is None or args.flow_weights is None) and not args.resume:
    print("WARNING: USING LATE FUSION WITHOUT PRE-LOADED WEIGHTS")

if USE_LF:
    ssd_net = build_ssd('train', size, num_classes, num_channels, no_rgb, args.rgb_weights, args.flow_weights, v3=USE_V3)
elif USE_DF:
    ssd_net = build_ssd('train', size, num_classes, num_channels, no_rgb, args.rgb_weights)
elif USE_V3:
    ssd_net = build_ssd('train', size, num_classes, num_channels, no_rgb, kernel_sz)
else:
    ssd_net = build_ssd('train', size, num_classes, num_channels, no_rgb)
net = ssd_net

optimizer = optim.SGD(net.parameters(), lr=args.lr,
                      momentum=args.momentum, weight_decay=args.weight_decay)
criterion = MultiBoxLoss(num_classes, 0.5, True, 0, True, 3, 0.5, False, args.cuda)

if args.cuda:
    net = torch.nn.DataParallel(ssd_net)
    cudnn.benchmark = True

if args.resume:
    print('Resuming training, loading {}...'.format(args.resume))
    if args.start_iter == 0:
        print("WARNING: WEIGHTS SPECIFIED BUT START_ITER IS 0")
    c = torch.load(args.resume)
    optimizer.load_state_dict(c['opt'].state_dict())
    ssd_net.load_state_dict(c['net'])
'''else:
    vgg_weights = torch.load(args.save_folder + args.basenet)
    print('Loading base network...')
    ssd_net.vgg.load_state_dict(vgg_weights)
'''
if args.cuda:
    net = net.cuda()

def xavier(param):
    init.xavier_uniform(param)


def weights_init(m):
    if isinstance(m, nn.Conv2d):
        xavier(m.weight.data)
        m.bias.data.zero_()

if not args.resume:
    print('Initializing weights...')
    # initialize newly added layers' weights with xavier method
    ssd_net.extras.apply(weights_init)
    ssd_net.loc.apply(weights_init)
    ssd_net.conf.apply(weights_init)


dataset = TVHands(open("/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/splits/trainIDs.txt").read().splitlines(),
    			"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/clips",
    			"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/pseudo_annos",
			"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/flows",
			SSDAugmentation(size, means),
			channels, scale_flow=scale_flow, percent_ds=percent_training, augment=augment, gt_scale=GT_SCALE
)
data_loader = data.DataLoader(dataset, batch_size, num_workers=args.num_workers,
			  shuffle=True, collate_fn=detection_collate, pin_memory=True)

validation_dataset = TVHands(
	open("/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/splits/validIDs.txt").read().splitlines(),
	"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/clips",
    	"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/pseudo_annos",
	"/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/flows",
	BaseTransform(size, means),
	channels, scale_flow=scale_flow, gt_scale=GT_SCALE
)
validation_data_loader = data.DataLoader(validation_dataset, batch_size, num_workers=args.num_workers,
                                  shuffle=True, collate_fn=detection_collate)

def train():
    net.train()
    # loss counters
    loc_loss = 0  # epoch
    conf_loss = 0
    epoch = 0
    print('Loading Dataset...')

    #dataset = VOCDetection(args.voc_root, train_sets, SSDAugmentation(
    #    ssd_dim, means), AnnotationTransform())

    epoch_size = len(dataset) // args.batch_size
    val_epoch_size = len(validation_dataset) // args.batch_size
    #print('Training SSD on', dataset.name)
    step_index = 0
    if args.visdom:
        # initialize visdom loss plot
        lot = viz.line(
            X=torch.zeros((1,)).cpu(),
            Y=torch.zeros((1, 3)).cpu(),
            opts=dict(
                xlabel='Iteration',
                ylabel='Loss',
                title='Current SSD Training Loss',
                legend=['Loc Loss', 'Conf Loss', 'Loss']
            )
        )
        val = viz.line(
            X=torch.zeros((1,)).cpu(),
            Y=torch.zeros((1, 3)).cpu(),
            opts=dict(
                xlabel='Iteration',
                ylabel='Val_Loss',
                title='Current SSD Validation Loss',
                legend=['Val Loc Loss', 'Val Conf Loss', 'Val Loss']
            )
        )
        epoch_lot = viz.line(
            X=torch.zeros((1,)).cpu(),
            Y=torch.zeros((1, 3)).cpu(),
            opts=dict(
                xlabel='Epoch',
                ylabel='Loss',
                title='Epoch SSD Training Loss',
                legend=['Loc Loss', 'Conf Loss', 'Loss']
            )
        )
    batch_num = 0
    batch_iterator = None
    for iteration in range(args.start_iter, max_iter):
        if (not batch_iterator) or (iteration % epoch_size == 0):
            # create batch iterator
            batch_iterator = iter(data_loader)
            if args.visdom:
                viz.line(
                    X=torch.ones((1, 3)).cpu() * epoch,
                    Y=torch.Tensor([loc_loss, conf_loss,
                        loc_loss + conf_loss]).unsqueeze(0).cpu() / epoch_size,
                    win=epoch_lot,
                    update='append'
                )
            # reset epoch loss counters
            loc_loss = 0
            conf_loss = 0
            epoch += 1
            batch_num = 0
        step_index = sum([1 if iteration >= x else 0 for x in stepvalues])
        adjust_learning_rate(optimizer, args.gamma, step_index)

        # load train data
        batch_num += 1
        images, targets = next(batch_iterator)
        #print(images.size())
        #exit(0)
        if args.cuda:
            images = Variable(images.cuda())
            targets = [Variable(anno.cuda(), volatile=True) for anno in targets]
        else:
            images = Variable(images)
            targets = [Variable(anno, volatile=True) for anno in targets]
        # forward
        t0 = time.time()
        out = net(images)
        # backprop
        optimizer.zero_grad()
        loss_l, loss_c = criterion(out, targets)
        loss = loss_l + loss_c
        loss.backward()
        optimizer.step()
        t1 = time.time()
        loc_loss += loss_l.data[0]
        conf_loss += loss_c.data[0]
        if iteration % 10 == 0:
            #print('iter ' + repr(iteration) + ' || Loss: %.4f ||' % (loss.data[0]), end=' ')
            if args.visdom and args.send_images_to_visdom:
                random_batch_index = np.random.randint(images.size(0))
                viz.image(images.data[random_batch_index].cpu().numpy())
        if args.visdom:
            viz.line(
                X=torch.ones((1, 3)).cpu() * iteration,
                Y=torch.Tensor([loss_l.data[0], loss_c.data[0],
                    loss_l.data[0] + loss_c.data[0]]).unsqueeze(0).cpu(),
                win=lot,
                update='append'
            )
            # hacky fencepost solution for 0th epoch plot
            if iteration == 0:
                viz.line(
                    X=torch.zeros((1, 3)).cpu(),
                    Y=torch.Tensor([loc_loss, conf_loss,
                        loc_loss + conf_loss]).unsqueeze(0).cpu(),
                    win=epoch_lot,
                    update=True
                )
        if iteration % args.save_val_every == 0 and iteration != 0:
            print('Saving state, iter:', iteration)
            save_loc = os.path.join(args.save_folder, 'ssd300_0712_iter_' + repr(iteration) + '.pth')
            print("Saving at: {}".format(save_loc))
            torch.save({'net': ssd_net.state_dict(), 'opt': optimizer}, save_loc)
            net.eval()
            val_bi = iter(validation_data_loader)
            loss_l_avg = 0
            loss_c_avg = 0
            for i in range(val_epoch_size):
                images, targets = next(val_bi)
                if args.cuda:
                    images = Variable(images.cuda())
                    targets = [Variable(anno.cuda()) for anno in targets]
                else:
                    images = Variable(images)
                    targets = [Variable(anno) for anno in targets]
                out = net(images)
                loss_l, loss_c = criterion(out, targets)
                loss_l_avg += loss_l.data[0]
                loss_c_avg += loss_c.data[0]
            loss_l_avg /= val_epoch_size
            loss_c_avg /= val_epoch_size
            print(iteration, "loss_l: {}, loss_c: {}".format(loss_l_avg, loss_c_avg))
            if args.visdom:
                viz.line(
                    X=torch.ones((1, 3)).cpu() * iteration,
                    Y=torch.Tensor([loss_l_avg, loss_c_avg,
                        loss_l_avg + loss_c_avg]).unsqueeze(0).cpu(),
                    win=val,
                    update='append'
                )

            net.train()



def adjust_learning_rate(optimizer, gamma, step):
    """Sets the learning rate to the initial LR decayed by 10 at every specified step
    # Adapted from PyTorch Imagenet example:
    # https://github.com/pytorch/examples/blob/master/imagenet/main.py
    """
    lr = args.lr * (gamma ** (step))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


if __name__ == '__main__':
    train()
