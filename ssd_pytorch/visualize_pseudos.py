import ast
import cv2
import glob
import random
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
import scipy.io as sio
from aps import get_box, get_trues, IoU

def draw_box(img, bx, color0=(0, 1, 0), color1=(0, 0, 1)):
	l = [(bx[0], bx[2]), (bx[1], bx[2]), (bx[1], bx[3]), (bx[0], bx[3])]
	l = [(round(x[0]), round(x[1])) for x in l]
	cv2.line(img, l[0], l[1], color0, 2)
	cv2.line(img, l[2], l[1], color1, 2)
	cv2.line(img, l[2], l[3], color1, 2)
	cv2.line(img, l[0], l[3], color1, 2)


RESULTS_DIR = "/home/[REDACTED]/Downloads/openpose/results"
IMG_DIR = "/nfs/[REDACTED]/[REDACTED]/DataSets/TVHands/v2/clips"

def run_once(video_id):
	s = video_id
	frame_num = sorted(os.listdir(os.path.join(IMG_DIR, s)))[0]
	path = os.path.join(IMG_DIR, s, frame_num, "frm_008.png")
	pr = ast.literal_eval(open(os.path.join(RESULTS_DIR, s+"_keypoints.json")).read())

	img = plt.imread(path)
	cmap = matplotlib.cm.get_cmap('Spectral')
	_, ax = plt.subplots(1)
	ax.set_aspect('equal')

	boxes = []

	for tr in get_trues(s):
		draw_box(img, tr, (1, 0, 1), (0, 1, 1))
		ax.text(tr[1], tr[3]+5, "{}".format(len(boxes)), bbox={'alpha': .3, 'facecolor': (0, 1, 1)})
		boxes.append(tr)
	num_true = len(boxes)

	for p in pr['people']:
		t = p['hand_right_keypoints']
		assert(len(t) % 3 == 0)
		bx = get_box(t)
		for x in range(len(t)//3):
			ax.add_patch(Circle((t[3*x], t[3*x+1]), 3, color=cmap(t[3*x+2])))
		if len(bx):
			draw_box(img, bx)
			ax.text(bx[1], bx[3], "{}".format(len(boxes)), bbox={'alpha': .3, 'facecolor': (1, 0, 1)})
			boxes.append(bx)

		t = p['hand_left_keypoints']
		assert(len(t) % 3 == 0)
		bx = get_box(t)
		for x in range(len(t)//3):
			ax.add_patch(Circle((t[3*x], t[3*x+1]), 3, color=cmap(t[3*x+2])))
		if len(bx):
			draw_box(img, bx)
			ax.text(bx[1], bx[3], "{}".format(len(boxes)), bbox={'alpha': .5, 'facecolor': (1, 0, 1)})
			boxes.append(bx)
			
	for i in range(num_true):
		for j in range(num_true, len(boxes)):
			x = IoU(boxes[i], boxes[j])
			assert(x == IoU(boxes[j], boxes[i]))
			if x:
				print("IoU of {}, {}: {}".format(i, j, x))
	print("-----------")
	ax.imshow(img)
	plt.show()

if __name__ == "__main__":
	if len(sys.argv) > 1:
		s = sys.argv[1]
	else:
		y = list(glob.glob(os.path.join("results_old", "*")))
		random.shuffle(y)
		for i in y:
			x = os.path.basename(i)
			run_once(x)
	run_once(s)

