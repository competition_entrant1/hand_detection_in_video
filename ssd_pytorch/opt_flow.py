import cv2
import os
import pickle
import multiprocessing
import glob
import my_utils as utils
import numpy as np

from joblib import delayed, Parallel

def opt_flow(img1, img2):
	f1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
	f2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
	
	f = cv2.calcOpticalFlowFarneback(f1, f2, None, .5, 3, 15, 3, 5, 1.2, 0)

	#x = cv2.normalize(f[..., 0], None, 0, 255, cv2.NORM_MINMAX).astype("uint8")
	#y = cv2.normalize(f[..., 1], None, 0, 255, cv2.NORM_MINMAX).astype("uint8")
	x = f[..., 0]
	y = f[..., 1]

	return (x, y) # horizontal, vertical

def opt_flow_from_id(x, j):
	p = "/home/[REDACTED]/project/opt_flows/{}".format(x+"|"+str(j))
	p1 = utils.get_path(x, j)
	p2 = utils.get_path(x, j+1)
	img1 = cv2.imread(p1)
	img2 = cv2.imread(p2)
	try:
		r = opt_flow(img1, img2)
		pickle.dump(r, open(p, "wb"))
	except:
		print("error: {}".format(p))
		with open("errors2.txt", "a") as f:
			f.write(p+"\n")

if __name__ == "__main__":
	a = utils.get_video_ids()
	Parallel(n_jobs=multiprocessing.cpu_count(), verbose=5)(
		delayed(opt_flow_from_id)(x, j) for x in a for j in range(1, 16) if not os.path.isfile("/home/[REDACTED]/project/opt_flows/{}".format(x+"|"+str(j)))
	)

